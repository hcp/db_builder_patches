/*
* org.nrg.xnat.restlet.XNATApplication
* XNAT http://www.xnat.org
* Copyright (c) 2013, Washington University School of Medicine
* All Rights Reserved
*
* Released under the Simplified BSD.
*
* Last modified 7/10/13 8:40 PM
*/
package org.nrg.xnat.turbine.modules.screens;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.search.TableSearch;

import java.util.Hashtable;

/**
 * @author Tim
 *
 */
public class XDATScreen_report_xnat_subjectData extends SecureReport {
	static Logger logger = Logger.getLogger(XDATScreen_report_xnat_mrSessionData.class);

    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    public void finalProcessing(RunData data, Context context) {
        try {
            XnatSubjectdata sub = new XnatSubjectdata(item);
            context.put("subject",sub);

            String dbname = ((XDATUserDetails)((VelocityContext) context).internalGet("user")).getDBName();
            String login = ((XDATUserDetails)((VelocityContext) context).internalGet("user")).getLogin();
            String query = "SELECT xnat_subjectdata.id, xnat_subjectdata.label FROM xnat_subjectdata RIGHT JOIN (SELECT DISTINCT subject_id FROM xnat_subjectassessordata WHERE id in (SELECT id from hcp_restrictedtier1 WHERE restricted_motherid = (SELECT DISTINCT restricted_motherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+sub.getId()+"')) AND restricted_fatherid = (SELECT DISTINCT restricted_fatherid FROM hcp_restrictedtier1 WHERE id in (SELECT id FROM xnat_subjectassessordata WHERE subject_id = '"+sub.getId()+"'))) AND subject_id <> '"+sub.getId()+"') AS siblings ON xnat_subjectdata.id = subject_id;";

            XFTTable table = TableSearch.Execute(query, dbname, login);
            Object siblingsList = null;
            if (table.size()>0)
            {
                table.resetRowCursor();
                int rowCount = 0;
                while(table.hasMoreRows())
                {
                    rowCount++;
                    Hashtable row = table.nextRowHash();
                    if(rowCount<=3){
                        context.put("sibling"+rowCount,row.get("id"));
                        context.put("siblinglabel"+rowCount,row.get("label"));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("",e);
        }
    }
}
